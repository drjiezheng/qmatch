# Quick Match

This is a package with some 2-d match functions used in astronomy work.

By Dr Jie Zheng and Dr Lin-qiao Jiang, from 2019

Upload at Sep 2023

## `match`

A fast 2-d match function, with a time complexity of O(nlogn+mlogm).

## `findneighbor`

The main part of the match function. Been picked out so other program can call it.

## `match2d`

A simple 2-d match function, but has time and space complexity of O(m*n). It works well when m or n is small.

## `offset1d`

A function find out offset of each dimension. For 2-d image align, can be used twice for x and y.

## `make_tri` & `match_triangle`

Making triangles by given points. As a preparation step of triangle matching in an astrometric process.

Match triangles from stars (point sources) of two images.

## `mean_xy` and `mean_offset1d`

A image matching method by mean of rows and cols. It will find the best integer offset between two images without finding stars.
